﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_3_and_4
{
    class DataConsolePrinter
    {
        //Task no. 3:
        public void PrintToConsole(IDataset data)
        {
            IReadOnlyCollection<List<string>> dataList = data.GetData();
            foreach (List<string> childList in dataList)
            {
                foreach (string item in childList)
                {
                    Console.Write(item + ",");
                }
                Console.WriteLine();
            }
            if (dataList == null)
            {
                Console.WriteLine("There is no data.");
                return;
            }
        }
    }
}
