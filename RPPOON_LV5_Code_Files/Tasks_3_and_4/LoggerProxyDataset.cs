﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Tasks_3_and_4
{
    //Task no. 4:
    class LoggerProxyDataset : IDataset
    {
        private string filePath;
        private Dataset dataset;
        private ConsoleLogger logger;
        DateTime time = new DateTime();

        public LoggerProxyDataset(string filePath)
        {
            this.filePath = filePath;
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            return dataset.GetData();
        }
        public void LogToConsole()
        {
            ReadOnlyCollection<List<string>> dataList = dataset.GetData();
            foreach (List<string> childList in dataList)
            {
                foreach (string item in childList)
                {
                    logger.Log(item, DateTime.Now);
                }
                Console.WriteLine();
            }
            if (dataList == null)
            {
                Console.WriteLine("There is no data.");
                return;
            }
        }
    }
}
