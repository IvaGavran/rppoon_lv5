﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Tasks_3_and_4
{
    //Task no. 4:
    class ConsoleLogger
    { 
        private static ConsoleLogger instance;

        private ConsoleLogger() {}
        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
            {
                instance = new ConsoleLogger();
            }
            return instance;
        }
        
        public void Log(string message, DateTime time)
        {
            Console.WriteLine(message + time);
        }
    }
}
