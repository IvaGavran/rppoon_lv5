﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_3_and_4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task no. 3:
            User user = User.GenerateUser("Iva");
            Dataset dataset = new Dataset("C:\\Users\\Kivi\\source\\repos\\RPPOON_LV5\\RPPOON_LV5\\FileLV5.csv");
            ProtectionProxyDataset protectionProxyDataset = new ProtectionProxyDataset(user);
            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset("C:\\Users\\Kivi\\source\\repos\\RPPOON_LV5\\RPPOON_LV5\\FileLV5.csv");
            DataConsolePrinter consolePrinter = new DataConsolePrinter();
            consolePrinter.PrintToConsole(dataset);
            Console.WriteLine();
            consolePrinter.PrintToConsole(protectionProxyDataset);
            Console.WriteLine();
            consolePrinter.PrintToConsole(virtualProxyDataset);
            Console.WriteLine();

            //Task no. 4:
            LoggerProxyDataset loggerProxyDataset = new LoggerProxyDataset("C:\\Users\\Kivi\\source\\repos\\RPPOON_LV5\\RPPOON_LV5\\FileLV5.csv");
            loggerProxyDataset.LogToConsole();
        }
    }
}
