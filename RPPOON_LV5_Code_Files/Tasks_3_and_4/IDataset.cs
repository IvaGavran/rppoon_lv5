﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Tasks_3_and_4
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
