﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_1_and_2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShipable> shippingPriceList = new List<IShipable>();
            ShippingService price = new ShippingService(7);
            Box box = new Box("My box");
            Product firstProduct = new Product("Product #1", 77.77, 10);
            Product secondProduct = new Product("Product #2", 77.77, 7);
            Product thirdProduct = new Product("Product #3", 77.77, 3);
            shippingPriceList.Add(box);
            shippingPriceList.Add(firstProduct);
            shippingPriceList.Add(secondProduct);
            shippingPriceList.Add(thirdProduct);
            double totalWeight = 0.0;

            foreach (IShipable item in shippingPriceList)
            {

                totalWeight+= item.Weight;
                Console.WriteLine(item.Description());

            }
            Console.WriteLine( "Shipping price: " + price.ShippingPrice(totalWeight));
        }
    }
}
