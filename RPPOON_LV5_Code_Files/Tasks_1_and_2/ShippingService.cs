﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_1_and_2
{
    //Task no. 2:
    class ShippingService
    {
        private double pricePerMass;

        public ShippingService(double pricePerMass)
        {
            this.pricePerMass = pricePerMass;
        }

        public double ShippingPrice(double weight)
        {
            return weight * pricePerMass;
        }
    }
}
