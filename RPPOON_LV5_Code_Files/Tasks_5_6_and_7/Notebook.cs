﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_5_6_and_7
{
    //Task no. 7:
    class Notebook
    {
        private List<Note> notes;
        ITheme theme;
        public Notebook(ITheme theme)
        {
            this.notes = new List<Note>();
            this.theme = theme;
        }
        public void AddNote(Note note) { this.notes.Add(note); }
        public void ChangeTheme(ITheme theme)
        {
            foreach (Note note in this.notes)
            {
                note.Theme = theme;
            }
        }
        public void Display()
        {
            foreach (Note note in this.notes)
            {
                note.Show();
                Console.WriteLine("\n");
            }
        }
    }
}
