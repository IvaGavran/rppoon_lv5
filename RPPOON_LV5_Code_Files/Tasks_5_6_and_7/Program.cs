﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_5_6_and_7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task no. 5:
            ITheme myTheme = new MyTheme();
            ReminderNote reminderNote = new ReminderNote("My message", myTheme);
            reminderNote.Show();
            Console.WriteLine();

            //Task no. 6:
            ITheme lightTheme = new LightTheme();
            GroupNote groupNoteOne = new GroupNote("Members of a group no. one:", myTheme);
            GroupNote groupNoteTwo = new GroupNote("Members of a group no. two:", lightTheme);

            groupNoteOne.Add("Iva");
            groupNoteOne.Add("Ivo");

            groupNoteTwo.Add("Bruno");
            groupNoteTwo.Add("Borna");

            groupNoteOne.Show();
            Console.WriteLine();
            groupNoteTwo.Show();
            Console.WriteLine();

            //Task no. 7:

            Notebook notebook = new Notebook(lightTheme);
            notebook.AddNote(groupNoteTwo);
            notebook.Display();

            notebook.ChangeTheme(myTheme);
            notebook.Display();
        }
    }
}
