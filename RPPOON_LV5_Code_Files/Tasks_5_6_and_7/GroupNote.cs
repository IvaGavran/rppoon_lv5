﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_5_6_and_7
{
    //Task no. 6:
    class GroupNote : Note
    {

        private List<String> members;
        public GroupNote(string message, ITheme theme) : base(message, theme) { members = new List<String>(); }
        public void Add(String member)
        {
            members.Add(member);
        }
        public void Remove(int member)
        {
            members.RemoveAt(member);
        }
        public override void Show()
        {
            this.ChangeColor();
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);
            foreach (String member in members)
            {
                Console.WriteLine(member);
            }
            Console.ResetColor();
        }
    }
}
